#! /usr/bin/env false

use v6.d;

use Pod::To::Anything::Subsets;

#| A base role for writing a Perl 6 Pod formatters with. It contains render
#| methods to take care of some plumbing efforts, so all you need to do is
#| define some render methods for the Pod blocks you want to give special
#| attention.
unit role Pod::To::Anything;

#| A catch method for lists of Pod objects, as you generally find them in
#| C<$=pod>. It will loop through all these documents and join them
#| together as a single document.
multi method render (@pod --> Str) {
	my $i = 0;
	my @outputs;

	# Initialize the subclass through the init method
	self.init() if self.^can('init');

	# Walk through all Pod documents and traverse them as needed
	for @pod -> $pod {
		my $prev;
		my $cur;
		my $next;

		loop {
			$prev = $cur // Pod::Block;
			$cur = $pod.contents[$i];
			$next = $pod.contents[++$i] // Pod::Block;

			@outputs.append(self.render($cur, :$prev, :$next));

			last if $pod.contents.elems ≤ $i;
		}
	}

	# Add the render-before output
	@outputs.prepend(self.render-before());

	# Add the render-after output if defined
	@outputs.append(self.render-after());

	# Join together all parts and trim whitespace
	@outputs.join.trim
}

method render-before (--> Str) { '' }
method render-after (--> Str) { '' }

#| Return nothing, as comments are not ment to be processed.
multi method render (Pod::Block::Comment $, Pod::Block :$prev, Pod::Block :$next --> Str) { '' }
multi method render (Pod::FormattingCode::Z:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { '' }

#| If the object given is just a plain Str, return it unmodified. If you
#| need to escape certain characters in all text, this is the place to do
#| it.
multi method render (Str:D $pod, Pod::Block :$prev, Pod::Block :$next --> Str) { $pod }

# These render methods cover all available Pod constructs. These should
# all be implemented to ensure your Pod formatter will work as intended.
multi method render (Pod::Block::Code:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::Block::Declarator:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::Block::Named::Author:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::Block::Named::Name:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::Block::Named::Subtitle:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::Block::Named::Title:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::Block::Named::Version:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::Block::Para:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::Block::Table:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::FormattingCode::B:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::FormattingCode::C:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::FormattingCode::E:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::FormattingCode::I:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::FormattingCode::K:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::FormattingCode::L:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::FormattingCode::N:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::FormattingCode::P:D $, Pod::Block :$prev, Pod::Block :$next--> Str) { … }
multi method render (Pod::FormattingCode::R:D $, Pod::Block :$prev, Pod::Block :$next--> Str) { … }
multi method render (Pod::FormattingCode::T:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::FormattingCode::U:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::FormattingCode::V:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::FormattingCode::X:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::Heading:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }
multi method render (Pod::Item:D $, Pod::Block :$prev, Pod::Block :$next --> Str) { … }

#| Retrieve the parsed contents from a Pod object. This is a helper method to
#| traverse Pod6 objects.
method traverse (Any:D $pod --> Str) {
	$pod.contents.map({ self.render($_) }).join.trim
}

#| Unpod a Pod element, turning it into plain ol' text instead.
method unpod (Any:D $pod --> Str) {
	$pod.contents.map(*.contents).join(' ')
}

=begin pod

=NAME    Pod::To::Anything
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 EXAMPLES

=head1 SEE ALSO

=end pod

# vim: ft=perl6 noet
